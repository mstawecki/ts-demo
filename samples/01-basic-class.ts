{

    /** This is a "Vehicle" class: */
    class Vehicle {

        name: string; // class property

        /** contructor - called when class object is created */
        constructor(name: string) {
            this.name = name;
        }

        /** class method - you can call it on an object */
        move() {
            console.log(this.name+" Moving...");
        }
    }

    var v1 = new Vehicle("Car"); // create object of Vehicle class
    v1.move(); // execute move() method.

}