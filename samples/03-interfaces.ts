
interface Point {
    x: number;
    y: number;
}

let pointData = { x: 1, y: 5, name:"a point" };
let point: Point = pointData;

let notPointData = { name:"Test" };
// point = notPointData; // Error: Type '{ name: string; }' is missing the following properties from type 'Point': x, y

let printPoint = (p: Point) => {
    console.log(p.x, p.y);
}

printPoint(point);
// printPoint(notPointData); // Error: Argument of type '{ name: string; }' is not assignable to parameter of type 'Point'.

// point.z = 9; // Error: Property 'z' does not exist on type 'Point'.