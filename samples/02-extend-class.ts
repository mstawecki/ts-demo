{

    /** This is a "Vehicle" class: */
    class Vehicle {

        name: string; // class property

        /** contructor - called when class object is created */
        constructor(name: string) {
            this.name = name;
        }

        /** class method - you can call it on an object */
        move() {
            console.log(this.name+" Moving...");
        }
    }

    class Bicycle extends Vehicle {
        wheels = 2;
        ring() {
            console.log(this.name+" Ringing...");
        }
    }

    var c1 = new Bicycle("Cycle"); // create object of Vehicle class
    c1.move(); // execute move() method from Vehicle
    c1.ring(); // execute ring() method from Bicycle

}